# jch - The Java selector for your shell

## Installation

Either

- download the realease archive and extract it
- or git-clone

to `~/.local/share/jch/`.

## Setup

Put this into your shell startup files like `~/.profile` or `~/.bashrc`

```bash
# define wrapper shell function
eval "$(~/.local/share/jch/bin/jch init)"

# define prompt action (optional, but recommended)
# bash:
[[ -n $PROMPT_COMMAND ]] && PROMPT_COMMAND+=';'
PROMPT_COMMAND+="jch apply >/dev/null 2>&1"
```

