#!/bin/bash

script_file=$(readlink -f "$0")
proj_dir=$(dirname "${script_file%/*}")

/bin/bash --rcfile <(cat <<-EOF
	eval "\$("$proj_dir/bin/jch" init)"
	[[ -n \$PROMPT_COMMAND ]] && PROMPT_COMMAND+=';'
	PROMPT_COMMAND+="jch apply >/dev/null 2>&1"
EOF
)

